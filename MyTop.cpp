#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iterator>
#include <cstdlib>
#include <unistd.h>
#include <dirent.h>

#include "Process.h"
#include "Users.h"

using namespace std;

const string cpu = "cpu";


//checks if folder name is numberic or not
bool isNumeric(char* dirName)
{
    char* iter = dirName;
    while (*iter != '\0')
    {
        if (*iter < '0' || *iter > '9')
            return false;
        iter++;
    }
    return true;
}

int main(int argc, const char * argv[]) {
    
    cout << "PID" << "\t" <<
    "USER" << "\t" <<
    "RES" << "\t" <<
    "%CPU" << "\t" <<
    "COMMAND" << endl;
    
    vector<Process> processes;
    
    //get all directories that are numbers and translate them
    struct dirent* dirEntity = NULL;
    DIR* dir_proc = opendir("/proc/");
    if (dir_proc == NULL)
    {
        cout << "couldnt open /proc dir" << endl;
        return 1;
    }
    
    
    // Loop on all dirs in /proc
    while ((dirEntity = readdir(dir_proc)))
    {
        if (dirEntity->d_type == DT_DIR)
        {
            if (isNumeric(dirEntity->d_name))
            {
                int pid = atoi(dirEntity->d_name);
                //create process with pid, it'll self initialize with its relevant values
                Process process(pid);
                
                //take sample of its current cpu time
                process.sampleSchedTime();
                
                processes.push_back(process);
            }
        }
    }
    
    closedir(dir_proc);
    //wait a sec
    sleep(1);
    
    
    //take second sample for all processes
    for (size_t i = 0; i < processes.size(); i++)
    {
        processes[i].calcCpuUsage();
    }
    
    //print all processes, done separately because printouts will spend
    //more cpu time and make processes's calcCpuUsage become a bit inaccurate
    for (size_t i = 0; i < processes.size(); i++)
    {
        processes[i].print();
    }
    
    //ugly, but not important for now
    //I should have used other pattern that self destructs but out of scope
    Users::getInstance()->~Users();
    return 0;
}