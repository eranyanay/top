#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include "Users.h"

using namespace std;

Users* Users::_instance = NULL;

Users* Users::getInstance()
{
    static Users* _instance;
    if (!_instance)
    {
        _instance = new Users();
    }
    return _instance;
}


string Users::getUser(int uid)
{
    return _mapUidToUser[uid];
}

Users::Users()
{
    //read all /etc/passwd users to map
    ifstream infile("/etc/passwd");
    
    if (infile)
    {
        string line;
        string name;
        int id;
        while (getline(infile, line))
        {
            // line is root:x:0:0:root:/root:/bin/bash
            //get user
            size_t userEndIdx = line.find(':', 0);
            name = line.substr(0, userEndIdx);
            
            //find its uid
            size_t uidStartIdx = line.find(':', userEndIdx + 1) + 1;
            size_t uidEndIdx = line.find(':', uidStartIdx);
            id = atoi(line.substr(uidStartIdx, uidEndIdx - uidStartIdx).c_str());
            
            //insert to uid to name map
            _mapUidToUser[id] = name;
        }
        infile.close();
    }
}

Users::~Users()
{
    if (_instance)
    {
        delete _instance;
    }
}
