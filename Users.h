#ifndef __mytop__Users__
#define __mytop__Users__

#include <cstdio>
#include <map>

class Users {
    std::map<int, std::string> _mapUidToUser;
    static Users* _instance;
    
public:
    static Users* getInstance();
    std::string getUser(int uid);
    ~Users();
private:
    Users();
};

#endif /* defined(__mytop__Users__) */
