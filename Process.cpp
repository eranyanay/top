#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <iterator>
#include <cstdlib>
#include <unistd.h>
#include <dirent.h>
#include <iomanip>

#include "Process.h"
#include "Users.h"
#include <time.h>

using namespace std;

const string name = "Name";
const string vmRss = "VmRSS";
const string uid = "Uid";


Process::Process(int pid)
{
    _nPid = pid;
    initMemAndUser();
    initProcessName();
}

void Process::sampleSchedTime()
{
    _nPrevTimes = readCpuTime();
}

//calculates cpu usage in percentage
//receives cpu titalTimediff
//period of time since sampleSchedTime is the same as for totalTimeDiff
void Process::calcCpuUsage()
{
    long currTimes = readCpuTime();
    _nCpuUsage = 100 * ((currTimes - _nPrevTimes) / (double)sysconf(_SC_CLK_TCK));
}

void Process::print()
{
    cout << _nPid << "\t" <<
    _sOwner << "\t" <<
    _nResidentSetSize << "KB\t" <<
    _nCpuUsage << "%\t" <<
    _sName << endl;
}


//reads from /proc/pid/status and fetches process name, physcal memory and owner
void Process::initMemAndUser()
{
    //init from /proc/pid/status
    stringstream ss;
    ss << "/proc/" << _nPid << "/status";
    ifstream infile(ss.str().c_str());
    
    if (infile)
    {
        string line;
        string var;
        while (getline(infile, line)) {
            istringstream iss (line);
            iss >> var;
            
            if (!var.compare(0, name.length(), name))
            {
                iss >> _sName;
            }
            else if (!var.compare(0, vmRss.length(), vmRss))
            {
                iss >> _nResidentSetSize;
            }
            else if (!var.compare(0, uid.length(), uid))
            {
                int userId;
                iss >> userId;
                _sOwner = Users::getInstance()->getUser(userId);
            }
        }
        infile.close();
    }
}

//tries to fetch the complete command line of process
void Process::initProcessName()
{
    //init process name & path from /proc/pid/cmdline
    stringstream ss;
    ss << "/proc/" << _nPid << "/cmdline";
    ifstream infile(ss.str().c_str());

    if (infile)
    {
        string line;
        getline(infile, line);
        if (line.length() > 0)
        {
            _sName = line;
        }
        infile.close();
    }
}

//reads cpu times utime + stime
long Process::readCpuTime()
{
    /*
     utime %lu   (14) Amount of time that this process has been scheduled in user mode, measured  in  clock
                        ticks  (divide by sysconf(_SC_CLK_TCK)).  This includes guest time, guest_time (time spent
                        running a virtual CPU, see below), so that applications that are not aware  of  the  guest
                        time field do not lose that time from their calculations.
     
     stime %lu   (15) Amount of time that this process has been scheduled in kernel mode, measured in clock
                        ticks (divide by sysconf(_SC_CLK_TCK)).
     
     */
    long time = 0;
    stringstream ss;
    ss << "/proc/" << _nPid << "/stat";
    ifstream infile(ss.str().c_str());
    if (infile)
    {
        //get line from file
        string line;
        getline(infile, line);
        infile.close();
        
        //take 14th-17th values and calculate them
        ss.str(line);
        istream_iterator<string> begin(ss);
        istream_iterator<string> end;
        vector<string> vals(begin, end);
        
        time = atol(vals[13].c_str()) + atol(vals[14].c_str());
    }
    return time;
}


