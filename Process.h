#ifndef __mytop__Process__
#define __mytop__Process__

#include <cstdio>
#include <string>
class Process {
    std::string _sName;         // Process name
    std::string _sOwner;        // User who created the process
    int _nPid;                  // Process ID
    int _nResidentSetSize;      // Memory used by process in KB
    long _nCpuUsage;          // CPU usage in %
    long _nPrevTimes;
    
public:
    Process(int pid);
    void print();
    void sampleSchedTime();
    void calcCpuUsage();
    
private:
    void initMemAndUser();
    void initProcessName();
    long readCpuTime();
};

#endif /* defined(__mytop__Process__) */
